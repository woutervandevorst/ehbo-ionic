import { Component , ViewChild } from '@angular/core';
import { NavController , AlertController } from 'ionic-angular';


//pages
import {LoginPage} from '../login/login';
import {RegisterPage} from '../register/register';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //splash = true;
  
//gegevens uit inputs halen
  @ViewChild('username') uname;
  @ViewChild('password') pword;

  constructor(public navCtrl: NavController , public alertCtrl: AlertController) {

  }
  ionViewDidLoad() {
   /* setTimeout(() => {
      this.splash = false;
    }, 4000);*/
    
  }

  signIn(){
    this.navCtrl.push(LoginPage);
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }
}
