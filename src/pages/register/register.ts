import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController, Platform } from 'ionic-angular';
//natives
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

//pages
import {LoggedinPage} from '../loggedin/loggedin'
import { User } from '../../models/user';
import { ProfilePage } from '../profile/profile';


//db
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';




/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user = {} as User;
  @ViewChild("passwordcheck") pass;
    
  constructor(private toast : ToastController,  private platform: Platform,  private fire: AngularFireAuth, private androidPermissions: AndroidPermissions, public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController) {
    if (this.platform.is('cordova')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.INTERNET).then(
        success => console.log('Permission granted'),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.INTERNET)
      );
      
    } 
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

 async registerUser(user : User){

    try{
      //controle op de passwoorden
      if (this.pass.value == user.password ){
        //create the user
        const result = await this.fire.auth.createUserWithEmailAndPassword(user.email, user.password);
        this.navCtrl.setRoot(ProfilePage);
      }else{
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: "passwoorden verschillend",
          buttons: ['ok']
        });
        alert.present();
      }
     

    }catch (e){
      console.error(e);
      let alert = this.alertCtrl.create({
        title: "Error",
        subTitle: e,
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }
  

}
