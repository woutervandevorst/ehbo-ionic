import { Component , ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
//firebase
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase} from 'angularfire2/database';
//profile model
import { Profile } from '../../models/profile';7
//native
import { AndroidPermissions } from '@ionic-native/android-permissions';
//page
import { LoggedinPage } from '../loggedin/loggedin';



/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profile = {} as Profile;
  //word achteraf gebruikt om data uit de inputs te halen
@ViewChild('firstName') first;
@ViewChild('lastName') last;
@ViewChild('birth') birth;
@ViewChild('blood') blood;
@ViewChild('med') med;
@ViewChild('phone') phone;
@ViewChild('alergic') alergic;

  constructor( private androidPermissions : AndroidPermissions , private alertCtrl : AlertController ,private platform: Platform , private fdb : AngularFireDatabase , private fire : AngularFireAuth , public navCtrl: NavController, public navParams: NavParams) {
    if (this.platform.is('cordova')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.INTERNET).then(
        success => console.log('Permission granted'),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.INTERNET)
      );
      
    } 
  }

  ionViewDidLoad() {  
    console.log('ionViewDidLoad ProfilePage');
  }

  createProfile(profile){
    //controle of alle velden zijn ingevuld, dit is veplicht wegens database tables
    if ( this.phone.value != "" && this.first.value != "" && this.last.value != "" && this.birth.value != "" && this.blood.value != "" && this.med.value != "" && this.alergic.value != ""){
      try{
        this.fire.authState.take(1).subscribe(auth =>{
          this.fdb.object(auth.uid+'/').set(this.profile)
            .then(() => this.navCtrl.setRoot(LoggedinPage))
          } )
          console.log(this.profile);
  
      }catch (e){
        
      }
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: "Gelieve alle venster in te vullen, als u niets heeft zet dan een /" ,
        buttons: ['OK']
      });
      alert.present();
    }
    
   
  }

}
