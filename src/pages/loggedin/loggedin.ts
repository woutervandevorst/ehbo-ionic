import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Platform} from 'ionic-angular';
//firebase
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
//natives
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { SMS } from '@ionic-native/sms';
import { CallNumber } from '@ionic-native/call-number';
import { Flashlight } from '@ionic-native/flashlight';
//pages
import { HomePage } from '../home/home';
import { UserPage } from '../user/user';





/**
 * Generated class for the LoggedinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loggedin',
  templateUrl: 'loggedin.html',
})
export class LoggedinPage {

  profileData;
  
  
  

  constructor( private callNumber : CallNumber , private sms : SMS , private flashlight : Flashlight , private androidPermissions : AndroidPermissions , private platform : Platform , private toast : ToastController, private fdb : AngularFireDatabase ,private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
    if (this.platform.is('cordova')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.INTERNET).then(
        success => console.log('Permission granted'),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.INTERNET)
      );
      
    
    } 
    try{
      //haal data Op uit database
      const personRef = this.fdb.database.ref(this.fire.auth.currentUser.uid);
      personRef.on('value' , personSnapshot =>{
        console.log(personSnapshot.val());
        this.profileData = personSnapshot.val();
        

      })
      //data validatie
      this.fire.authState.take(1).subscribe(data =>{
        if (data && data.email && data.uid){
          this.toast.create({
            message: 'welkom op deze EHBO app ',
            duration : 3000
          }).present();
  
          
          
           
          
         
  
  
        }else {
          this.toast.create({
            message: "kon uw gegevens niet ophalen",
            duration: 3000
          }).present();
        }
      });
    }catch (e){
      console.log(e);
    }
  }
      
  
  

  ionViewDidLoad() {
    
 
  }
  signoutuser() {
    this.fire.auth.signOut();
    this.navCtrl.setRoot(HomePage)
  }
  OpenProfile(){
    this.navCtrl.push(UserPage);
  }

  Emergency(){
    //in deze functie worden de natives uitgevoerd
    if (this.platform.is('cordova')) {
    this.flashlight.toggle();
    //dit is een statisch telefoon nummer maar normaal zou dat 112 zijn maar wegens testing moeilijkheden heb ik hier mijn nummer ingezet.
    this.callNumber.callNumber("0474800441" , true)
    .then(() => console.log ("er word gebeld"))
    .catch(() => console.log( "kapot"));
  //telefoon nummer word opgehaald uit de database
    this.sms.send(this.profileData.PhoneNumber,"Help ik zit in de problemen, dit is geen grap maar verstuurd via een ehbo app");

    
    }else{
      //als het niet op gsm word gebruikt word dit uitgevoerd
      this.toast.create({
        message: "het nodige is gedaan voor u",
        duration: 3000
      }).present();
    }
  }

}
