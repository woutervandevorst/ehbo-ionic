import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform,  AlertController } from 'ionic-angular';
//natives
import { AndroidPermissions } from '@ionic-native/android-permissions';
//firebase
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
//page
import { LoggedinPage } from '../loggedin/loggedin';

/**
 * Generated class for the EdituserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edituser',
  templateUrl: 'edituser.html',
})
export class EdituserPage {

  profileData;
  //data uit inputs
  @ViewChild('firstName') first;
  @ViewChild('lastName') last;
  @ViewChild('birth') birth;
  @ViewChild('blood') blood;
  @ViewChild('med') med;
  @ViewChild('phone') phone;
  @ViewChild('alergic') alergic;

  constructor( private alertCtrl : AlertController, private androidPermissions : AndroidPermissions , private platform : Platform , private fdb : AngularFireDatabase , private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
    if (this.platform.is('cordova')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.INTERNET).then(
        success => console.log('Permission granted'),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.INTERNET)
      );
      
     
    } 
    try{
      
      const personRef = this.fdb.database.ref(this.fire.auth.currentUser.uid);
      personRef.on('value' , personSnapshot =>{
        
        this.profileData = personSnapshot.val();
        console.log(this.profileData);
     
      });


      
    }catch (e){
      console.log(e);
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EdituserPage');
  }

  UpdateUser(){
    try{
      //check op ingevulde velden
      if ( this.phone.value != "" && this.first.value != "" && this.last.value != "" && this.birth.value != "" && this.blood.value != "" && this.med.value != "" && this.alergic.value != ""){
      //haal data van gebruiker op
        this.fdb.database.ref(this.fire.auth.currentUser.uid).on('value' , eventSnapshot => {
          //update van de gegevens
        this.fdb.database.ref(this.fire.auth.currentUser.uid).update({
          PhoneNumber : this.phone.value,
          alergic: this.alergic.value,
          birthYear: this.birth.value,
          bloodGroup: this.blood.value,
          firstName: this.first.value,
          lastName: this.last.value,
          medication: this.med.value,
        })
      });
      this.navCtrl.pop();
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'gelieven alle velden in te geven, als u niets heeft geef dan een "/" in' ,
        buttons: ['OK']
      });
      alert.present();
    }

    }catch (e){
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'er is iets fout gelopen, probeer later opnieuw',
        buttons: ['OK']
      });
      alert.present();
    }
    

  }

}
