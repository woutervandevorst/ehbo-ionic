import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoggedinPage } from '../loggedin/loggedin';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { User } from '../../models/user';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {


  user =  {} as User;

  
  constructor(private androidPermissions: AndroidPermissions,private platform: Platform, private fire:AngularFireAuth, public navCtrl: NavController, public navParams: NavParams ,public alertCtrl: AlertController) {
    if (this.platform.is('cordova')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.INTERNET).then(
        success => console.log('Permission granted'),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.INTERNET)
      );
      
    } 
    
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  

  async signInUser(user : User){
    let alert = this.alertCtrl.create({
      title: 'Something Wrong',
      subTitle: 'email of paswoord is verkeerd',
      buttons: ['OK']
    });
    
    try{
      this.fire.auth.signInWithEmailAndPassword(user.email ,  user.password)
      .then(() => 
    this.navCtrl.setRoot(LoggedinPage)
    )
    .catch(() =>
      alert.present()
  ); 
      
      
    }catch (e){
      console.log(e);
    }
    
  }

  

}
