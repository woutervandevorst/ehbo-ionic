import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

//natives
import { AndroidPermissions } from '@ionic-native/android-permissions';

//firebase
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
//pages
import { EdituserPage } from '../edituser/edituser';


/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  profileData = [];

  constructor( private fdb : AngularFireDatabase, private fire : AngularFireAuth , private androidPermissions : AndroidPermissions , private platform : Platform , public navCtrl: NavController, public navParams: NavParams) {
    if (this.platform.is('cordova')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.INTERNET).then(
        success => console.log('Permission granted'),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.INTERNET)
      );
      
    } 

    try{
      //gegevens van gebruiker ophalen
      const personRef = this.fdb.database.ref(this.fire.auth.currentUser.uid);
      personRef.on('value' , personSnapshot =>{
        this.profileData = personSnapshot.val();
        console.log(this.profileData);

      })

    }catch(e){
      console.error(e);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  EditUser(){
    this.navCtrl.push(EdituserPage);
  }

}
